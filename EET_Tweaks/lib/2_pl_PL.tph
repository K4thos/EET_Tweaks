/////                                                  \\\\\
///// Edwin                                            \\\\\
/////                                                  \\\\\

// the same voice actor (Robert Płuszka) plays the role in both games, so there is no need for special treatment. Just added some sounds to BG1 CRE files that were introduced in BG2 (comments on nature, more attack taunts etc.)

// replace cre voice entries
ACTION_FOR_EACH npc IN EDWIN EDWIN2 EDWIN4 EDWIN6 E35 BEGIN
	ACTION_IF (FILE_EXISTS_IN_GAME ~%npc%.cre~) BEGIN
		COPY_EXISTING ~%npc%.cre~ ~override~
			PATCH_IF (SOURCE_SIZE > 0x2d3) BEGIN
				SAY ~0xcc~ #3967 // BATTLE_CRY2
				SAY ~0xd0~ #30694 // BATTLE_CRY3
				SAY ~0xd4~ #30706 // BATTLE_CRY4
				SAY ~0xf8~ #5345 // AREA_FOREST
				SAY ~0xfc~ #5346 // AREA_CITY
				SAY ~0x100~ #5347 // AREA_DUNGEON
				SAY ~0x104~ #5348 // AREA_DAY
				SAY ~0x108~ #5349 // AREA_NIGHT
				SAY ~0x118~ #30709 // SELECT_COMMON4
				SAY ~0x11c~ #30710 // SELECT_COMMON5
				SAY ~0x120~ #30711 // SELECT_COMMON6
				SAY ~0x1a0~ #30716 // SELECT_RARE1
				SAY ~0x1a4~ #30717 // SELECT_RARE2
				SAY ~0x1a8~ #30718 // CRITICAL_HIT
				SAY ~0x1ac~ #30719 // CRITICAL_MISS
				SAY ~0x1b0~ #30720 // TARGET_IMMUNE
				SAY ~0x1b4~ #30721 // INVENTORY_FULL
				SAY ~0x1b8~ #30722 // PICKED_POCKET
				SAY ~0x1bc~ #30723 // HIDDEN_IN_SHADOWS (EXISTANCE1)
				SAY ~0x1c0~ #30724 // SPELL_DISRUPTED (EXISTANCE2)
				SAY ~0x1c4~ #30725 // SET_A_TRAP (EXISTANCE3)
			END
		BUT_ONLY
	END
END

/////                                                  \\\\\
///// Imoen                                            \\\\\
/////                                                  \\\\\

// it's hard to believe but the same voice actress (Anna Dużyńska) played the role in both games. The problem is that in BG1 for whatever stupid reason she used Russian accent and sounds nothing like in BG2. That is why she will now use her BG2 voice set (from later part of game, not those pessimistic lines from Irenicus Dungeon). Remaining original BG1 tracks are replaced with dummy (silent) files.

// replace cre voice entries
ACTION_FOR_EACH npc IN IMOEN IMOEN1 IMOEN2 IMOEN4 IMOEN6_ BEGIN
	ACTION_IF (FILE_EXISTS_IN_GAME ~%npc%.cre~) BEGIN
		COPY_EXISTING ~%npc%.cre~ ~override~
			PATCH_IF (SOURCE_SIZE > 0x2d3) BEGIN
				SAY ~0xa4~ #30739 //INITIAL_MEETING
				SAY ~0xa8~ #30759 // MORALE
				SAY ~0xac~ #30726 // HAPPY
				SAY ~0xb0~ #4338 // UNHAPPY_ANNOYED
				SAY ~0xb4~ #30727 // UNHAPPY_SERIOUS
				SAY ~0xb8~ @4000000 // UNHAPPY_BREAKING_POINT
				SAY ~0xbc~ #4337 // LEADER
				SAY ~0xc0~ #4339 // TIRED
				SAY ~0xc4~ #30760 // BORED
				SAY ~0xc8~ #30761 // BATTLE_CRY1
				SAY ~0xcc~ #30762 // BATTLE_CRY2
				SAY ~0xd0~ #11035 // BATTLE_CRY3
				SAY ~0xd4~ #-1 // BATTLE_CRY4
				SAY ~0xec~ #5395 // DAMAGE
				SAY ~0xf0~ #5396 // DYING
				SAY ~0xf4~ #30764 // HURT
				SAY ~0xf8~ #30731 // AREA_FOREST
				SAY ~0xfc~ #30732 // AREA_CITY
				SAY ~0x100~ #11036 // AREA_DUNGEON
				SAY ~0x104~ #30733 // AREA_DAY
				SAY ~0x108~ #30734 // AREA_NIGHT
				SAY ~0x10c~ #30735 // SELECT_COMMON1
				SAY ~0x110~ #30736 // SELECT_COMMON2
				SAY ~0x114~ #30737 // SELECT_COMMON3
				SAY ~0x118~ #30738 // SELECT_COMMON4
				SAY ~0x11c~ #30739 // SELECT_COMMON5
				SAY ~0x120~ #30740 // SELECT_COMMON6
				SAY ~0x124~ #11038 // SELECT_ACTION1
				SAY ~0x128~ #11039 // SELECT_ACTION2
				SAY ~0x12c~ #4335 // SELECT_ACTION3
				SAY ~0x130~ #4336 // SELECT_ACTION4
				SAY ~0x134~ #11043 // SELECT_ACTION5
				SAY ~0x138~ #11042 // SELECT_ACTION6
				SAY ~0x13c~ #11045 // SELECT_ACTION7
				SAY ~0x1a0~ #30741 // SELECT_RARE1
				SAY ~0x1a4~ @4000001 // SELECT_RARE2
				SAY ~0x1a8~ #30746 // CRITICAL_HIT
				SAY ~0x1ac~ #30747 // CRITICAL_MISS
				SAY ~0x1b0~ #30748 // TARGET_IMMUNE
				SAY ~0x1b4~ #30749 // INVENTORY_FULL
				SAY ~0x1b8~ #30750 // PICKED_POCKET
				SAY ~0x1bc~ #30751 // HIDDEN_IN_SHADOWS (EXISTANCE1)
				SAY ~0x1c0~ #30752 // SPELL_DISRUPTED (EXISTANCE2)
				SAY ~0x1c4~ #30753 // SET_A_TRAP (EXISTANCE3)
			END
		BUT_ONLY
	END
END

// tortures are mentioned in original @30741 string and assigned voice track. It doesn't make sense to use it in BG1 so I edited it
COPY ~EET_Tweaks/2/pl_PL/IMOEN/IMOEN41_.wav~ ~override~

// dummy (silent) wavs are used for the remaining voices in BG1 portion of the game (quest/banter relevant ones). In case some mods attaches original voice tracks in dialogue I replaced all of them with dummy files
ACTION_FOR_EACH voice IN IMOEN01_ IMOEN02_ IMOEN03_ IMOEN04_ IMOEN05_ IMOEN06_ IMOEN07_ IMOEN08_ IMOEN09_ IMOEN10_ IMOEN11_ IMOEN12_ IMOEN13_ IMOEN14_ IMOEN15_ IMOEN16_ IMOEN17_ IMOEN18_ IMOEN19_ IMOEN20_ IMOEN21_ IMOEN22_ IMOEN23_ IMOEN24_ IMOEN25_ IMOEN26_ IMOEN27_ IMOEN28_ IMOEN30_ BEGIN
	ACTION_IF (FILE_EXISTS_IN_GAME ~%voice%.wav~) BEGIN
		COPY ~EET_Tweaks/2/dummy.wav~ ~override/%voice%.wav~
	END
END

/////                                                  \\\\\
///// Jaheira                                          \\\\\
/////                                                  \\\\\

// the same voice actress (Henryka Korzycka) plays the role in both games, but she did MUCH better job in BG2 (compare "For the fallen!" line for example). So her lines remains mostly unchanged, but voices for common actions has been replaced with BG2 ones if they were available.

// replace cre voice entries
ACTION_FOR_EACH npc IN JAHEIR JAHEIR2 JAHEIR4 JAHEIR6 E36 BEGIN
	ACTION_IF (FILE_EXISTS_IN_GAME ~%npc%.cre~) BEGIN
		COPY_EXISTING ~%npc%.cre~ ~override~
			PATCH_IF (SOURCE_SIZE > 0x2d3) BEGIN
				SAY ~0xa8~ #4008 // MORALE
				SAY ~0xac~ #4010 // HAPPY
				SAY ~0xb0~ #4011 // UNHAPPY_ANNOYED
				SAY ~0xb4~ #4012 // UNHAPPY_SERIOUS
				SAY ~0xb8~ #4013 // UNHAPPY_BREAKING_POINT
				SAY ~0xbc~ #4014 // LEADER
				SAY ~0xc0~ #4015 // TIRED
				SAY ~0xc4~ #4016 // BORED
				SAY ~0xc8~ #4009 // BATTLE_CRY1
				SAY ~0xcc~ #30765 // BATTLE_CRY2
				SAY ~0xd0~ #30766 // BATTLE_CRY3
				SAY ~0xec~ #5353 // DAMAGE
				SAY ~0xf0~ #5354 // DYING
				SAY ~0xf4~ #4017 // HURT
				SAY ~0xf8~ #4018 // AREA_FOREST
				SAY ~0xfc~ #4019 // AREA_CITY
				SAY ~0x100~ #4020 // AREA_DUNGEON
				SAY ~0x104~ #5352 // AREA_DAY
				SAY ~0x108~ #4021 // AREA_NIGHT
				SAY ~0x10c~ #4022 // SELECT_COMMON1
				SAY ~0x110~ #4023 // SELECT_COMMON2
				SAY ~0x114~ #54350 // SELECT_COMMON3
				SAY ~0x118~ #4024 // SELECT_COMMON4
				SAY ~0x11c~ #30767 // SELECT_COMMON5
				SAY ~0x120~ #30768 // SELECT_COMMON6
				SAY ~0x124~ #4025 // SELECT_ACTION1
				SAY ~0x128~ #30769 // SELECT_ACTION2
				SAY ~0x12c~ #4027 // SELECT_ACTION3
				SAY ~0x130~ #4028 // SELECT_ACTION4
				SAY ~0x134~ #4029 // SELECT_ACTION5
				SAY ~0x138~ #4030 // SELECT_ACTION6
				SAY ~0x13c~ #4031 // SELECT_ACTION7
				SAY ~0x1a0~ #30770 // SELECT_RARE1
				SAY ~0x1a4~ #30771 // SELECT_RARE2
				SAY ~0x1a8~ #30772 // CRITICAL_HIT
				SAY ~0x1ac~ #30775 // CRITICAL_MISS
				SAY ~0x1b0~ #30776 // TARGET_IMMUNE
				SAY ~0x1b4~ #30777 // INVENTORY_FULL
				SAY ~0x1b8~ #30778 // PICKED_POCKET
				SAY ~0x1bc~ #30779 // HIDDEN_IN_SHADOWS (EXISTANCE1)
				SAY ~0x1c0~ #30780 // SPELL_DISRUPTED (EXISTANCE2)
				SAY ~0x1c4~ #30781 // SET_A_TRAP (EXISTANCE3)
			END
		BUT_ONLY
	END
END

/////                                                  \\\\\
///// Minsc                                            \\\\\
/////                                                  \\\\\

// similar situation to Imoen - same voice actor (Janusz Marcinowicz), awesome BG2 performance, and totally different direction in BG1, where he sounds nothing like the English voice actor or his BG2 performance (I had to listen to his lines over and over to even believe that it is the same voice actor). Simply bland and even worse he is often mispronouncing Boo name in BG1 :( That is why all sounds are replaced with BG2 ones and remaining BG1 tracks are replaced with dummy (silent) files.

// replace cre voice entries
ACTION_FOR_EACH npc IN MINSC MINSC2 MINSC4 MINSC6 E32 BEGIN
	ACTION_IF (FILE_EXISTS_IN_GAME ~%npc%.cre~) BEGIN
		COPY_EXISTING ~%npc%.cre~ ~override~
			PATCH_IF (SOURCE_SIZE > 0x2d3) BEGIN
				SAY ~0xa4~ #22083 //INITIAL_MEETING
				SAY ~0xa8~ #30788 // MORALE
				SAY ~0xac~ #4087 // HAPPY
				SAY ~0xb0~ #4088 // UNHAPPY_ANNOYED
				SAY ~0xb4~ #4089 // UNHAPPY_SERIOUS
				SAY ~0xb8~ #4090 // UNHAPPY_BREAKING_POINT
				SAY ~0xbc~ #30790 // LEADER
				SAY ~0xc0~ #4092 // TIRED
				SAY ~0xc4~ #4093 // BORED
				SAY ~0xc8~ #4086 // BATTLE_CRY1
				SAY ~0xcc~ #30793 // BATTLE_CRY2
				SAY ~0xd0~ #30794 // BATTLE_CRY3
				SAY ~0xd4~ #-1 // BATTLE_CRY4
				SAY ~0xd8~ #-1 // BATTLE_CRY5
				SAY ~0xec~ #5359 // DAMAGE
				SAY ~0xf0~ #5360 // DYING
				SAY ~0xf4~ #4094 // HURT
				SAY ~0xf8~ #4095 // AREA_FOREST
				SAY ~0xfc~ #5357 // AREA_CITY
				SAY ~0x100~ #4096 // AREA_DUNGEON
				SAY ~0x104~ #5358 // AREA_DAY
				SAY ~0x108~ #4097 // AREA_NIGHT
				SAY ~0x10c~ #4098 // SELECT_COMMON1
				SAY ~0x110~ #4099 // SELECT_COMMON2
				SAY ~0x114~ #4100 // SELECT_COMMON3
				SAY ~0x118~ #30796 // SELECT_COMMON4
				SAY ~0x11c~ #30798 // SELECT_COMMON5
				SAY ~0x120~ #30799 // SELECT_COMMON6
				SAY ~0x124~ #4101 // SELECT_ACTION1
				SAY ~0x128~ #30800 // SELECT_ACTION2
				SAY ~0x12c~ #30801 // SELECT_ACTION3
				SAY ~0x130~ #4104 // SELECT_ACTION4
				SAY ~0x134~ #30806 // SELECT_ACTION5
				SAY ~0x138~ #30807 // SELECT_ACTION6
				SAY ~0x13c~ #4107 // SELECT_ACTION7
				SAY ~0x1a0~ #30808 // SELECT_RARE1
				SAY ~0x1a4~ #30809 // SELECT_RARE2
				SAY ~0x1a8~ #30937 // CRITICAL_HIT
				SAY ~0x1ac~ #31850 // CRITICAL_MISS
				SAY ~0x1b0~ #31901 // TARGET_IMMUNE
				SAY ~0x1b4~ #33805 // INVENTORY_FULL
				SAY ~0x1b8~ #34379 // PICKED_POCKET
				SAY ~0x1bc~ #34380 // HIDDEN_IN_SHADOWS (EXISTANCE1)
				SAY ~0x1c0~ #34381 // SPELL_DISRUPTED (EXISTANCE2)
				SAY ~0x1c4~ #34744 // SET_A_TRAP (EXISTANCE3)
			END
		BUT_ONLY
	END
END

// dummy (silent) wavs are used for the remaining voices in BG1 portion of the game (quest/banter relevant ones). In case some mods attaches original voice tracks in dialogue I replaced all of them with dummy files.
ACTION_FOR_EACH voice IN MINSC01_ MINSC02_ MINSC03_ MINSC04_ MINSC05_ MINSC06_ MINSC07_ MINSC08_ MINSC09_ MINSC10_ MINSC11_ MINSC12_ MINSC14_ MINSC16_ MINSC17_ MINSC18_ MINSC19_ MINSC20_ MINSC21_ MINSC22_ MINSC23_ MINSC24_ MINSC25_ MINSC26_ MINSC27_ MINSC28_ MINSC29_ MINSC30_ MINSC31_ MINSC32_ MINSC33_ MINSC34_ MINSC35_ MINSC36_ MINSC37_ MINSC38_ MINSC39_ MINSC40_ MINSC41_ BEGIN
	ACTION_IF (FILE_EXISTS_IN_GAME ~%voice%.wav~) BEGIN
		COPY ~EET_Tweaks/2/dummy.wav~ ~override/%voice%.wav~
	END
END

/////                                                  \\\\\
///// Viconia                                          \\\\\
/////                                                  \\\\\

// the same voice actress (Gabriela Kownacka) plays the role in both games, so she keeps all of her BG1 voices and receives additional sounds introduced in BG2 (see Edwin)

// replace cre voice entries
ACTION_FOR_EACH npc IN VICONI VICONI4 VICONI6_ BEGIN
	ACTION_IF (FILE_EXISTS_IN_GAME ~%npc%.cre~) BEGIN
		COPY_EXISTING ~%npc%.cre~ ~override~
			PATCH_IF (SOURCE_SIZE > 0x2d3) BEGIN
				SAY ~0xc8~ #3614 // BATTLE_CRY1
				SAY ~0xcc~ #29812 // BATTLE_CRY2
				SAY ~0xd0~ #29813 // BATTLE_CRY3
				SAY ~0xfc~ #5281 // AREA_CITY
				SAY ~0x118~ #30517 // SELECT_COMMON4
				SAY ~0x11c~ #30520 // SELECT_COMMON5
				SAY ~0x120~ #30524 // SELECT_COMMON6
				SAY ~0x1a0~ #30526 // SELECT_RARE1
				SAY ~0x1a4~ #30527 // SELECT_RARE2
				SAY ~0x1a8~ #30528 // CRITICAL_HIT
				SAY ~0x1ac~ #30529 // CRITICAL_MISS
				SAY ~0x1b0~ #30534 // TARGET_IMMUNE
				SAY ~0x1b4~ #30535 // INVENTORY_FULL
				SAY ~0x1b8~ #30536 // PICKED_POCKET
				SAY ~0x1bc~ #30537 // HIDDEN_IN_SHADOWS (EXISTANCE1)
				SAY ~0x1c0~ #30538 // SPELL_DISRUPTED (EXISTANCE2)
				SAY ~0x1c4~ #30539 // SET_A_TRAP (EXISTANCE3)
			END
		BUT_ONLY
	END
END

/////                                                  \\\\\
///// Wraith Sarevok                                   \\\\\
/////                                                  \\\\\

// touched up Sarevok's (Daniel Olbrychski) Hell Trials speech to be more in line with his BG1 performance and his current spiritual appearance. During Hell Trials Sarevok uses the same, fully armoured animation as in BG1, so it didn't make sense that the Wraith had crystal clear voice as in ToB, where he is alive and no longer wears helmet that distorts his voice.
COPY ~EET_Tweaks/2/pl_PL/HELSAR~ ~override~

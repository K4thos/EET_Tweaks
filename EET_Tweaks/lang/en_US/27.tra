//IWD:EE @3284 (capitalized)
@27000000 = ~Cadaverous undead~
//IWD:EE @3304 + @15991
@27000001 = ~Cadaverous undead
Cadaverous undead are a subclass of undead. All undead that consist primarily of flesh, such as ghouls, zombies, and wights, are considered to be cadaverous. They are the undead most likely to attack humans for cannibalistic purposes. Cadaverous undead are frequently slow moving and rely on melee attacks to take down their prey.

Ghouls are undead creatures, once human, who now feed on the flesh of corpses. Although the change from human to ghoul has deranged and destroyed their minds, ghouls have a terrible cunning which enables them to hunt their prey most effectively.

Ghouls are vaguely recognizable as once having been human, but have become horribly disfigured by their change to ghouls. The tongue becomes long and tough for licking marrow from cracked bones, the teeth become sharp and elongated, and the nails grow strong and sharp like claws.~
//IWD:EE @3279 (capitalized, singular)
@27000002 = ~Giant~
//IWD:EE @3291
@27000003 = ~Giant
Giants are well known throughout the Frozen North. Huge and powerful, giants are extremely dangerous foes. They use their strength and mass to fell opponents with a single blow. Giants come in a number of varieties. The most common giant race spoken of in Icewind Dale is the mighty Frost Giant.~
//IWD:EE @3280 (capitalized, singular)
@27000004 = ~Goblin~
//IWD:EE @3292
@27000005 = ~Goblin
Goblins are extremely common in many non-human areas of Faerûn. The foundation of the "goblinoid" races, many goblins typify everything that humans associate with their cousins: stupidity, greed, and wickedness. Goblins are not very technologically advanced. As a result, they frequently raid human settlements for food and other provisions.~
//IWD:EE @3281 (capitalized)
@27000006 = ~Lizard men~
//IWD:EE @3293
@27000007 = ~Lizard men
Typically only found in tropical regions, lizard men are primitive bipeds with a well developed social structure and advanced religious beliefs. Lizard men tribes are typically led by a lizard king who is significantly larger and more powerful than his kin. Lizard men do not come into conflict with humans often, but their fondness for the taste of human flesh often causes problems.~
//IWD:EE @3282 (capitalized, singular)
@27000008 = ~Orc~
//IWD:EE @3294
@27000009 = ~Orc
Orcs are the hereditary enemies of goblins and hobgoblins, with whom they often compete for resources. Reasonably intelligent carnivores, orcs are constantly waging war against other races to maintain their tribes. These wars thin the weak predators from their tribes and gather resources for the next generation. Orcs have a reputation for being extremely powerful warriors. Wizards have tried to create orcish variants to take advantage of the race's natural ferocity and strength.~
//IWD:EE @3290 (capitalized, singular)
@27000010 = ~Salamander~
//IWD:EE @3295
@27000011 = ~Salamander
Beings from the Elemental Plane of Fire and the Paraelemental Plane of Ice, salamanders are large creatures with the upper bodies of men and the lower bodies of serpents. Salamanders are typically only found in extremely hot and extremely cold environments. As these beings are elemental-kin, they radiate small amounts of elemental energy at all times. They are selfish and cruel creatures that cannot abide being removed from their environment for an extended period of time.~
//IWD:EE @3283 (capitalized)
@27000012 = ~Skeletal undead~
//IWD:EE @3296
@27000013 = ~Skeletal undead
Another classification of undead, skeletal undead are, perhaps, the most common form of undead foe. Necromancers and evil priests use skeletal minions as cheap shock troops when less powerful undead are not available. Some necromancers will use advanced magic to create skeletal undead that are tougher, faster, and more resilient than their common counterparts. Because of their composition, skeletal undead are usually very resistant to slashing attacks and almost immune to missile fire.~
//IWD:EE @3285 (capitalized)
@27000014 = ~Spectral undead~
//IWD:EE @3297
@27000015 = ~Spectral undead
Spectral undead are often considered to be the most dangerous form of undead because they are typically intelligent and quite powerful. Lacking a corporeal form, spectral undead often cannot be hit by non-magical weapons. Also, like most undead, they are largely unaffected by charm magic, cold, poison, and illusions. Spectral undead are usually tied to the material plane by a curse or magical effect. Ghosts, shadows, and wraiths are all common examples of spectral undead.~
//IWD:EE @3286 (capitalized, singular)
@27000016 = ~Spider~
//IWD:EE @3298
@27000017 = ~Spider
Monstrously large spiders create problems all over Faerûn. Some giant spiders are magically created and others are naturally occurring, but they all pose a potential threat to humanoid settlements. There are a wide variety of giant spiders, each with their own strengths and weaknesses. Giant spiders are feared for their powerful fangs and dangerous poison.~
//IWD:EE @3288 (capitalized, singular)
@27000018 = ~Troll~
//IWD:EE @3299
@27000019 = ~Troll
Some might argue that trolls are the natural enemy of most rangers. Inherently violent and aggressive, trolls frequently attack with little or no provocation. They attack humans and small human settlements regularly, destroying everything in their path. Trolls are not only feared for their large, powerful limbs, but for their ability to rapidly regenerate most wounds inflicted on them. Experienced adventurers carry fire and acid with them when expecting trolls.~
//IWD:EE @3289 (capitalized, singular)
@27000020 = ~Umber hulk~
//IWD:EE @3300
@27000021 = ~Umber hulk
Residents of the Underdark, umber hulks are huge, incredibly powerful monsters with thick, scaly hides, sharp mandibles, and two sets of eyes. They use strong talons to shred through rock at a frightening speed, often taking unsuspecting miners by surprise. The most debilitating power of the umber hulk is its ability to cause confusion with its gaze. Many adventurers have made their final mistake by looking into the eyes of an umber hulk.~
//IWD:EE @30850 (capitalized)
@27000022 = ~Yuan-ti~
//IWD:EE @3301
@27000023 = ~Yuan-ti
These creatures are rare, but extremely deadly opponents. Yuan-ti are an offshoot of a race which ruled Faerûn long before humanoids took control. With snakelike bodies and vaguely human limbs, yuan-ti hide in secret vaults until the time is right to strike out at their enemies. Yuan-ti are extremely intelligent foes with very sophisticated magical talents. They are known for their cunning minds, poisonous traps, and vile religious rituals.~
